#ifndef ATOMIC_QUEUE_STD_ATOMIC_HPP
#define ATOMIC_QUEUE_STD_ATOMIC_HPP

#pragma error("Not ready yet")
template<typename T>
class atomic_queue<std::atomic<T>> {
private:
    std::queue<std::atomic<T>>           m_queue;
public:
    constexpr atomic_queue() = default;

    constexpr void push(const std::atomic<T> &value) {
        m_queue.push(value);
    }

    constexpr std::atomic<T> pop() {
        std::atomic<T> front = m_queue.front();
        m_queue.pop();
        return front;
    }

    template <typename Rep, typename Period>
    constexpr std::optional<std::atomic<T>> poll(std::chrono::duration<Rep, Period> timeout) {
        if (m_queue.empty()) {
            return {};
        }

        T front = m_queue.front();
        m_queue.pop();
        T value = front;

        return value;
    }

    constexpr size_t size() {
        return m_queue.size();
    }

    [[nodiscard]] constexpr bool empty() {
        return m_queue.empty();
    }

    template< class... Args >
    constexpr decltype(auto) emplace( Args&&... args ) {
        return m_queue.emplace(args...);
    }
};

#endif // ATOMIC_QUEUE_STD_ATOMIC_HPP