#ifndef ATOMIC_QUEUE_ATOMIC_QUEUE_HPP
#define ATOMIC_QUEUE_ATOMIC_QUEUE_HPP

#include <optional>
#include <mutex>
#include <queue>
#include <condition_variable>
#include <type_traits>

/**
 * Synchronized, blocking queue for concurrent use by multiple threads
 * @tparam T the contained type
 */
template <typename T> class atomic_queue {
public:
    constexpr atomic_queue() = default;

    /**
     * Pushes a single element at the end of the queue (blocking)
     * @param value value to push
     */
    constexpr void push(const T &value) {
        std::lock_guard<std::mutex> lock(m_mutex);

        m_queue.push(value);
        m_cond_not_empty.notify_one();
    }

    /**
     * Pop the first element of the queue. Blocks if the queue is empty, wait for an element to consume (blocking)
     * @return T
     */
    [[nodiscard]] constexpr T pop() {
        std::unique_lock<std::mutex> lock(m_mutex);
        m_cond_not_empty.wait(lock, [this] { return !m_queue.empty(); });

        T front = m_queue.front();
        m_queue.pop();
        return front;
    }

    /**
     * Same as pop(), except that it blocks for at most %timeout
     * @param timeout How much time to wait before giving up
     * @return An std::optional<T>, containing the result if any.
     */
    template <typename Rep, typename Period>
    [[nodiscard]] constexpr std::optional<T> poll(std::chrono::duration<Rep, Period> timeout) {
        std::unique_lock<std::mutex> lock(m_mutex);
        if (m_cond_not_empty.wait_for(lock, timeout,
                                      [this] {return !m_queue.empty(); })) {
            if (m_queue.empty()) {
                return {};
            }

            T front = m_queue.front();
            m_queue.pop();
            T value = front;

            return value;
        }
        return {};
    }

    /**
     * @return The current size of the queue (blocking)
     */
    [[nodiscard]] constexpr size_t size() {
        std::lock_guard<std::mutex> lock(m_mutex);
        return m_queue.size();
    }

    /**
     * Returns true if the queue is empty (blocking)
     * @return
     */
    [[nodiscard]] constexpr bool empty() {
        std::lock_guard<std::mutex> lock(m_mutex);
        return m_queue.empty();
    }

    /**
     * Construct elements in-place at the end of the queue (blocking)
     * @tparam args template variadic arguments
     * @return
     */
    template< class... Args >
    constexpr decltype(auto) emplace( Args&&... args ) {
        std::lock_guard<std::mutex> lock(m_mutex);
        return m_queue.emplace(args...);
    }

private:
    std::queue<T>           m_queue;
    std::mutex              m_mutex;
    std::condition_variable m_cond_not_empty;
};


#endif //ATOMIC_QUEUE_ATOMIC_QUEUE_HPP
