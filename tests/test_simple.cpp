#include <atomic_queue/atomic_queue.hpp>
#include <thread>
#include <iostream>
#include <sstream>
#include <atomic>
#include <cassert>

std::vector<std::jthread> create_consumers(unsigned number, atomic_queue<int>& queue, volatile std::atomic<int>& total_sum) {
    using namespace std::chrono_literals;

    std::vector<std::jthread> consumers(number) ;
    for (std::jthread& thread : consumers) {
        thread = std::jthread([&queue, &total_sum] (const std::stop_token& stop_token) {
            int local_sum = 0;
            while (!stop_token.stop_requested() || !queue.empty()) {
                if (std::optional<int> value = queue.poll(100ms)) {
                    local_sum += value.value();
                }
            }
            std::stringstream ss;
            std::hash<std::thread::id> hasher;
            ss << "Consumer " << hasher(std::this_thread::get_id()) % 1000 << ": local_sum=" << local_sum << std::endl;
            std::cout << ss.str() << std::flush;
            total_sum += local_sum;
        });
    }
    std::cout << "Created " << consumers.size() << " consumers" << std::endl;

    return std::move(consumers);
}

std::vector<std::jthread> create_providers(unsigned number,
                                           atomic_queue<int>& queue,
                                           volatile std::atomic<int>& expected_total_sum,
                                           const unsigned item_number) {
    std::vector<std::jthread> providers(number) ;
    for (std::jthread& thread : providers) {
        thread = std::jthread([&queue, &expected_total_sum, item_number]() {
            for (int i = 0; i < item_number; i++) {
                queue.push(1);
            }
            expected_total_sum += item_number;
        });
    }
    std::cout << "Created " << providers.size() << " providers" << std::endl;

    return providers;
}

void test_configuration(unsigned consumers_number, unsigned providers_number, const unsigned item_number) {
    atomic_queue<int> queue;

    volatile std::atomic<int> total_sum = 0;
    volatile std::atomic<int> expected_total_sum = 0;

    using namespace std::chrono_literals;

    std::vector<std::jthread> consumers = create_consumers(consumers_number, queue, total_sum);

    std::vector<std::jthread> providers = create_providers(providers_number, queue, expected_total_sum, item_number);

    for (std::jthread& thread : providers) {
        thread.join();
    }
    std::cout << "Finished populating queue with " << expected_total_sum << " items" << std::endl;

    for (std::jthread& thread : consumers) {
        thread.request_stop();
    }

    for (std::jthread& thread : consumers) {
        thread.join();
    }

    std::cout << "Final result: " << total_sum << std::endl;

    assert(expected_total_sum == total_sum);
}

int main(int argc, char* argv[]) {


    test_configuration(16, 1, 10000);
    test_configuration(1, 16, 10000);
    test_configuration(16, 16, 10000);

    return EXIT_SUCCESS;
}